=== TinyChat Room Spy Extra ===

Contributors: ruddernation
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=FFAC7FBEBH6JE
Tags: tinychat-roomspy, tinychat, chat, room spy, wordpress chat, buddypress chat, wordpress video chat, buddypress video chat
Requires at least: 4.0
Tested up to: 4.5
Stable tag: 1.0.8
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html

== Description ==
This loads the room of your choice for TinyChat, change 'Your chosen room name' to your selected room name, This will install like a normal plugin and not conflict with my room spy plugin.

== Installation ==

This will automatically create the page and install the short code with link domain name/roomspy-extra, If it does not then please read below.

Simply use shortcode [room_spy_extra_page] in a page and publish.

== Screenshots ==

* This is the search bar, Enter the name and click "Spy".
* Once you clicked spy it will locate the room and pull the images and names of the users in the chat room, It'll tell you number of admin/chatters and on camera.

== Notes ==
This loads the room of your choice for TinyChat, change 'Your chosen room name' to your selected room name, This will install like a normal plugin and not conflict with my room spy plugin.
== Frequently Asked Questions ==

* Q. Can I use this if I'm not logged in?
* A. Yes!.

* Q. How do I add it to my blog/website?
* A. Just go to the backend and on appearance select menus, From there you can add your page, It'll be roomspy by default.

== Changelog ==

= 1.0.1 =
* first start, This will auto create the page for you and insert the shortcode, Just type in the Tinychat room name in the search box and click *Spy*,
It'll grab the images and selected data for who is in the room, number of users and admin and how many are using video/audio.

= 1.0.7 = 
* Major update to provide profile information of the room, Also included a minor css fix, Remember to change 'room_name' in wp-content/plugins/tinychat-roomspy-extra(-master)/room-spy.php, Then save and it should come up with your Tinychat room, it will auto refresh page every 20 seconds as well.

== Social Sites ==

* Website - https://www.ruddernation.com

* Room Spy - https://www.tinychat-spy.com

* Facebook - https://www.facebook.com/rndtc

* Github - https://github.com/ruddernation

* GitHub Repositories - https://ruddernation-designs.github.io

* Google Plus - https://plus.google.com/+Ruddernation/posts

* Google Community - https://plus.google.com/communities/100073731487570686181

* Twitter - https://twitter.com/_ruddernation_
